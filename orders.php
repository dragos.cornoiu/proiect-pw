<?php
// Include configuration file
@include 'config.php';

// Start session
session_start();

// Check if user is not logged in, redirect to login page
$user_id = $_SESSION['user_id'];
if (!isset($user_id)) {
    header('location:login.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your Orders</title>

    <!-- Font Awesome CDN link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">

    <!-- Custom admin CSS file link -->
    <link rel="stylesheet" href="css/style.css">

    <style>
        body {
            font-family: 'Arial', sans-serif;
            font-size: 18px;
            line-height: 1.6;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
            color: #333;
        }

        header {
            background-color: #333;
            color: #fff;
            padding: 15px;
            text-align: center;
            font-size: 24px;
        }

        section {
            padding: 20px;
            margin: 20px;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        .heading h3 {
            font-size: 36px;
            margin-bottom: 10px;
        }

        .heading p {
            font-size: 20px;
            color: #777;
        }

        .placed-orders .title {
            font-size: 28px;
            margin-bottom: 20px;
        }

        .box {
            border: 1px solid #ddd;
            padding: 15px;
            margin-bottom: 15px;
            border-radius: 8px;
        }

        .box p {
            font-size: 20px;
            margin-bottom: 8px;
        }

        .box span {
            font-weight: bold;
            color: #555;
        }

        .empty {
            font-size: 24px;
            color: #777;
        }

        footer {
            background-color: #333;
            color: #fff;
            padding: 15px;
            text-align: center;
            position: absolute;
            bottom: 0;
            width: 100%;
        }
    </style>
</head>

<body>

    <?php @include 'header.php'; ?>

    <section class="heading">
        <h3>Your Orders</h3>
        <p><a href="home.php">Home</a> / Order</p>
    </section>

    <section class="placed-orders">

        <h1 class="title">Placed Orders</h1>

        <div class="box-container">
            <?php
            // Retrieve orders for the user
            $select_orders = mysqli_query($conn, "SELECT * FROM `orders` WHERE user_id = '$user_id'") or die('Query failed');
            if (mysqli_num_rows($select_orders) > 0) {
                while ($fetch_orders = mysqli_fetch_assoc($select_orders)) {
            ?>
                    <div class="box">
                        <p>Placed on: <span><?php echo $fetch_orders['placed_on']; ?></span></p>
                        <p>Name: <span><?php echo $fetch_orders['name']; ?></span></p>
                        <p>Number: <span><?php echo $fetch_orders['number']; ?></span></p>
                        <p>Email: <span><?php echo $fetch_orders['email']; ?></span></p>
                        <p>Address: <span><?php echo $fetch_orders['address']; ?></span></p>
                        <p>Payment Method: <span><?php echo $fetch_orders['method']; ?></span></p>
                        <p>Your Orders: <span><?php echo $fetch_orders['total_products']; ?></span></p>
                        <p>Total Price: <span>$<?php echo $fetch_orders['total_price']; ?>/-</span></p>
                        <p>Payment Status: <span style="color:<?php echo ($fetch_orders['payment_status'] == 'pending') ? 'tomato' : 'green'; ?>"><?php echo $fetch_orders['payment_status']; ?></span></p>
                    </div>
            <?php
                }
            } else {
                echo '<p class="empty">No orders placed yet!</p>';
            }
            ?>
        </div>

    </section>

    <?php @include 'footer.php'; ?>

    <script src="js/script.js"></script>

</body>

</html>
