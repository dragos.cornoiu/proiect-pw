<?php

@include 'config.php';

session_start();

$admin_id = $_SESSION['admin_id'];

if (!isset($admin_id)) {
   header('location:login.php');
}

// Update Product Logic
if (isset($_POST['update_product'])) {
   $update_p_id = $_POST['update_p_id'];
   $name = mysqli_real_escape_string($conn, $_POST['name']);
   $price = mysqli_real_escape_string($conn, $_POST['price']);
   $details = mysqli_real_escape_string($conn, $_POST['details']);

   // Update product details
   mysqli_query($conn, "UPDATE `products` SET name = '$name', details = '$details', price = '$price' WHERE id = '$update_p_id'") or die('query failed');

   // Update product image if a new one is selected
   $image = $_FILES['image']['name'];
   $image_size = $_FILES['image']['size'];
   $image_tmp_name = $_FILES['image']['tmp_name'];
   $image_folter = 'uploaded_img/' . $image;
   $old_image = $_POST['update_p_image'];

   if (!empty($image)) {
      if ($image_size > 2000000) {
         $message[] = 'Image size is too large!';
      } else {
         mysqli_query($conn, "UPDATE `products` SET image = '$image' WHERE id = '$update_p_id'") or die('query failed');
         move_uploaded_file($image_tmp_name, $image_folter);
         unlink('uploaded_img/' . $old_image);
         $message[] = 'Image updated successfully!';
      }
   }

   // Update product categories
   $categories = isset($_POST['categories']) ? $_POST['categories'] : array();

   // Delete existing categories for the product
   mysqli_query($conn, "DELETE FROM `product_categories` WHERE product_id = '$update_p_id'") or die('query failed');

   // Insert new categories for the product
   foreach ($categories as $category_id) {
      mysqli_query($conn, "INSERT INTO `product_categories` (product_id, category_id) VALUES ('$update_p_id', '$category_id')") or die('query failed');
   }

   $message[] = 'Product updated successfully!';
}

// Fetch product details for update
if (isset($_GET['update'])) {
   $update_id = $_GET['update'];
   $select_product = mysqli_query($conn, "SELECT * FROM `products` WHERE id = '$update_id'") or die('query failed');
   $fetch_product = mysqli_fetch_assoc($select_product);
   $selected_categories = array();
   $select_categories = mysqli_query($conn, "SELECT category_id FROM `product_categories` WHERE product_id = '$update_id'") or die('query failed');
   while ($fetch_category = mysqli_fetch_assoc($select_categories)) {
      $selected_categories[] = $fetch_category['category_id'];
   }
} else {
   header('location:admin_products.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Update Product</title>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
   <link rel="stylesheet" href="css/admin_style.css">
</head>

<body>

   <?php @include 'admin_header.php'; ?>

   <section class="update-product">
      <form action="" method="post" enctype="multipart/form-data">
         <img src="uploaded_img/<?php echo $fetch_product['image']; ?>" class="image" alt="">
         <input type="hidden" value="<?php echo $fetch_product['id']; ?>" name="update_p_id">
         <input type="hidden" value="<?php echo $fetch_product['image']; ?>" name="update_p_image">
         <input type="text" class="box" value="<?php echo $fetch_product['name']; ?>" required placeholder="Update product name" name="name">
         <input type="number" min="0" class="box" value="<?php echo $fetch_product['price']; ?>" required placeholder="Update product price" name="price">
         <textarea name="details" class="box" required placeholder="Update product details" cols="30" rows="10"><?php echo $fetch_product['details']; ?></textarea>
         <input type="file" accept="image/jpg, image/jpeg, image/png" class="box" name="image">

         <!-- Multi-select dropdown for categories -->
         <select name="categories[]" class="box" multiple required>
            <?php
            $select_all_categories = mysqli_query($conn, "SELECT * FROM `categories`") or die('query failed');
            while ($fetch_all_category = mysqli_fetch_assoc($select_all_categories)) {
               $selected = in_array($fetch_all_category['cid'], $selected_categories) ? 'selected' : '';
               echo "<option value='{$fetch_all_category['cid']}' $selected>{$fetch_all_category['name']}</option>";
            }
            ?>
         </select>

         <input type="submit" value="Update Product" name="update_product" class="btn">
         <a href="admin_products.php" class="option-btn">Go Back</a>
      </form>
   </section>

   <script src="js/admin_script.js"></script>

</body>

</html>
