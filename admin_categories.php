<?php

@include 'config.php';

session_start();

$admin_id = $_SESSION['admin_id'];

if (!isset($admin_id)) {
    header('location:login.php');
}

// Add Category Logic
if (isset($_POST['add_category'])) {
    $category_name = mysqli_real_escape_string($conn, $_POST['category_name']);

    $select_category_name = mysqli_query($conn, "SELECT name FROM `categories` WHERE name = '$category_name'") or die('query failed');

    if (mysqli_num_rows($select_category_name) > 0) {
        $message[] = 'Category name already exists!';
    } else {
        $insert_category = mysqli_query($conn, "INSERT INTO `categories` (name) VALUES ('$category_name')") or die('query failed');

        if ($insert_category) {
            $message[] = 'Category added successfully!';
        }
    }
}

// Delete Category Logic
if (isset($_GET['delete_category'])) {
    $delete_category_id = $_GET['delete_category'];

    $check_category_association = mysqli_query($conn, "SELECT COUNT(*) as count FROM `product_categories` WHERE category_id = '$delete_category_id'") or die('query failed');
    $association_count = mysqli_fetch_assoc($check_category_association)['count'];

    if ($association_count == 0) {
        mysqli_query($conn, "DELETE FROM `categories` WHERE cid = '$delete_category_id'") or die('query failed');
        $message[] = 'Category deleted successfully!';
    } else {
        $message[] = 'Cannot delete category. It is associated with products.';
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Categories</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    <link rel="stylesheet" href="css/admin_style.css">
</head>

<body>

    <?php @include 'admin_header.php'; ?>

    <section class="add-categories">
        <form action="" method="POST">
            <h3>Add New Category</h3>
            <input type="text" class="box" required placeholder="Enter category name" name="category_name">
            <input type="submit" value="Add Category" name="add_category" class="btn">
        </form>
    </section>

    <section class="show-categories">
        <div class="box-container">
            <?php
            $select_categories = mysqli_query($conn, "SELECT * FROM `categories`") or die('query failed');
            if (mysqli_num_rows($select_categories) > 0) {
                while ($fetch_category = mysqli_fetch_assoc($select_categories)) {
            ?>
                    <div class="box">
                        <div class="name"><?php echo $fetch_category['name']; ?></div>
                        <a href="admin_categories.php?delete_category=<?php echo $fetch_category['cid']; ?>" class="delete-btn" onclick="return confirm('Delete this category?');">delete</a>
                    </div>
            <?php
                }
            } else {
                echo '<p class="empty">No categories added yet!</p>';
            }
            ?>
        </div>
    </section>

    <script src="js/admin_script.js"></script>

</body>

</html>
