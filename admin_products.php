<?php
@include 'config.php';

session_start();

$admin_id = $_SESSION['admin_id'];

if (!isset($admin_id)) {
    header('location:login.php');
}

// Add Product Logic
if (isset($_POST['add_product'])) {
    $name = mysqli_real_escape_string($conn, $_POST['name']);
    $price = mysqli_real_escape_string($conn, $_POST['price']);
    $details = mysqli_real_escape_string($conn, $_POST['details']);
    $image = $_FILES['image']['name'];
    $image_size = $_FILES['image']['size'];
    $image_tmp_name = $_FILES['image']['tmp_name'];
    $image_folter = 'uploaded_img/' . $image;
    $categories = isset($_POST['categories']) ? $_POST['categories'] : array();

    $select_product_name = mysqli_query($conn, "SELECT name FROM `products` WHERE name = '$name'") or die('query failed');

    if (mysqli_num_rows($select_product_name) > 0) {
        $message[] = 'Product name already exists!';
    } else {
        $insert_product = mysqli_query($conn, "INSERT INTO `products`(name, details, price, image) VALUES('$name', '$details', '$price', '$image')") or die('query failed');

        if ($insert_product) {
            if ($image_size > 2000000) {
                $message[] = 'Image size is too large!';
            } else {
                move_uploaded_file($image_tmp_name, $image_folter);
                $product_id = mysqli_insert_id($conn);
                $failed_categories = array();

                foreach ($categories as $category_id) {
                    // Check if the category_id exists in the categories table
                    $check_category_query = mysqli_query($conn, "SELECT cid FROM categories WHERE cid = '$category_id'");
                    
                    if ($check_category_query && mysqli_num_rows($check_category_query) > 0) {
                        // Category_id exists, insert into product_categories
                        mysqli_query($conn, "INSERT INTO `product_categories` (product_id, category_id) VALUES ('$product_id', '$category_id')") or $failed_categories[] = $category_id;
                    } else {
                        // Category_id does not exist, add to the failed_categories array
                        $failed_categories[] = $category_id;
                    }
                }

                if (empty($failed_categories)) {
                    $message[] = 'Product added successfully!';
                } else {
                    $failed_categories_str = implode(', ', $failed_categories);
                    $message[] = "Some categories (IDs: $failed_categories_str) do not exist and were not added to the product.";
                }
            }
        }
    }
}

// Delete Product Logic
if (isset($_GET['delete'])) {
    $delete_id = $_GET['delete'];
    // Fetch image name before deletion
    $select_delete_image = mysqli_query($conn, "SELECT image FROM `products` WHERE id = '$delete_id'") or die('query failed');
    $fetch_delete_image = mysqli_fetch_assoc($select_delete_image);

    // Delete categories associated with the product
    mysqli_query($conn, "DELETE FROM `product_categories` WHERE product_id = '$delete_id'") or die('query failed');

    // Delete the product
    mysqli_query($conn, "DELETE FROM `products` WHERE id = '$delete_id'") or die('query failed');
    mysqli_query($conn, "DELETE FROM `wishlist` WHERE pid = '$delete_id'") or die('query failed');
    mysqli_query($conn, "DELETE FROM `cart` WHERE pid = '$delete_id'") or die('query failed');

    // Delete the product image after successful deletion
    unlink('uploaded_img/' . $fetch_delete_image['image']);

    header('location:admin_products.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Products</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    <link rel="stylesheet" href="css/admin_style.css">
    <style>
        body {
            font-family: 'Arial', sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
            color: #333;
        }

        /* Add your custom styles here */
        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
        }

        .box {
            margin-bottom: 20px;
        }

        .box-container {
            display: flex;
            flex-wrap: wrap;
            gap: 20px;
        }

        .box img {
            max-width: 100%;
            height: auto;
            border-radius: 8px;
        }

        .categories span {
            display: inline-block;
            margin-right: 5px;
            background-color: #333;
            color: #fff;
            padding: 5px;
            border-radius: 4px;
        }

        .btn {
            background-color: #4caf50;
            color: #fff;
            border: none;
            padding: 10px 20px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
            border-radius: 4px;
        }

        .empty {
            font-size: 18px;
            color: #777;
        }
    </style>
</head>

<body>

    <?php @include 'admin_header.php'; ?>

    <div class="container">
        <section class="add-products">
            <form action="" method="POST" enctype="multipart/form-data">
                <h2>Add New Product</h2>
                <input type="text" class="box" required placeholder="Product Name" name="name">
                <input type="number" min="0" class="box" required placeholder="Product Price" name="price">
                <textarea name="details" class="box" required placeholder="Product Details" cols="30" rows="10"></textarea>
                <input type="file" accept="image/jpg, image/jpeg, image/png" required class="box" name="image">

                <!-- Multi-select dropdown for categories -->
                <select name="categories[]" class="box" multiple required>
                    <?php
                    $select_categories = mysqli_query($conn, "SELECT * FROM `categories`") or die('query failed');
                    while ($fetch_category = mysqli_fetch_assoc($select_categories)) {
                        echo "<option value='{$fetch_category['cid']}'>{$fetch_category['name']}</option>";
                    }
                    ?>
                </select>

                <input type="submit" value="Add Product" name="add_product" class="btn">
            </form>
        </section>

        <section class="show-products">
            <div class="box-container">
                <?php
                $select_products = mysqli_query($conn, "SELECT * FROM `products`") or die('query failed');
                if (mysqli_num_rows($select_products) > 0) {
                    while ($fetch_products = mysqli_fetch_assoc($select_products)) {
                ?>
                        <div class="box">
                            <div class="price">$<?php echo $fetch_products['price']; ?>/-</div>
                            <img class="image" src="uploaded_img/<?php echo $fetch_products['image']; ?>" alt="">
                            <div class="name"><?php echo $fetch_products['name']; ?></div>
                            <div class="details"><?php echo $fetch_products['details']; ?></div>

                            <!-- Displaying Categories -->
                            <div class="categories">
                                <?php
                                $product_id = $fetch_products['id'];
                                $select_product_categories = mysqli_query($conn, "SELECT c.name FROM categories c
                                    INNER JOIN product_categories pc ON c.cid = pc.category_id
                                    WHERE pc.product_id = '$product_id'") or die('query failed');

                                while ($fetch_category = mysqli_fetch_assoc($select_product_categories)) {
                                    echo '<span class="category"> ' . $fetch_category['name'] . '</span>';
                                }
                                ?>
                            </div>

                            <a href="admin_update_product.php?update=<?php echo $fetch_products['id']; ?>" class="btn">Update</a>
                            <a href="admin_products.php?delete=<?php echo $fetch_products['id']; ?>" class="btn" onclick="return confirm('Delete this product?');">Delete</a>
                        </div>
                <?php
                    }
                } else {
                    echo '<p class="empty">No products added yet!</p>';
                }
                ?>
            </div>
        </section>
    </div>

    <script src="js/admin_script.js"></script>

</body>

</html>