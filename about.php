<?php
@include 'config.php';

session_start();

$user_id = $_SESSION['user_id'];

if (!isset($user_id)) {
    header('location:login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About Us</title>

    <!-- Font Awesome CDN link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">

    <!-- Custom CSS file link for the sneakers store -->
    <link rel="stylesheet" href="css/style.css">

    <style>
        body {
            font-family: 'Roboto', sans-serif;
            background: linear-gradient(to right, #fdfbfb, #ebedee);
            color: #495057;
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            line-height: 1.6;
        }

        section {
            padding: 60px 0;
        }

        .heading {
            background: linear-gradient(to right, #343a40, #2c3036);
            color: #ffffff;
            text-align: center;
            padding: 20px;
        }

        .heading h3 {
            font-size: 36px;
            margin-bottom: 10px;
        }

        .heading p {
            font-size: 18px;
            color: #ced4da;
        }

        .about {
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .flex {
            display: flex;
            justify-content: space-between;
            flex-wrap: wrap;
            margin-top: 40px;
        }

        .content {
            flex: 1;
            max-width: 600px;
            text-align: center;
            padding: 20px;
            background: #ffffff;
            border-radius: 10px;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
        }

        .content h3 {
            font-size: 28px;
            margin-bottom: 10px;
            color: #343a40;
        }

        .content p {
            font-size: 18px;
            margin-bottom: 20px;
            color: #495057;
        }

        .btn {
            display: inline-block;
            background-color: #007bff;
            color: #ffffff;
            padding: 10px 20px;
            font-size: 18px;
            text-decoration: none;
            border-radius: 5px;
            transition: background-color 0.3s ease;
        }

        .btn:hover {
            background-color: #0056b3;
        }

        .image img {
            max-width: 100%;
            height: auto;
            border-radius: 5px;
        }

        .reviews {
            background: linear-gradient(to right, #fdfbfb, #ebedee);
        }

        .title {
            text-align: center;
            font-size: 36px;
            color: #343a40;
            margin-bottom: 40px;
        }

        .box-container {
            display: flex;
            justify-content: space-around;
            flex-wrap: wrap;
            margin-top: 20px;
        }

        .box {
            flex: 1;
            max-width: 300px;
            background: #ffffff;
            padding: 20px;
            margin: 20px;
            border-radius: 10px;
            text-align: center;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
        }

        .box img {
            max-width: 100%;
            height: auto;
            border-radius: 50%;
            margin-bottom: 20px;
        }

        .box p {
            font-size: 16px;
            margin-bottom: 20px;
            color: #495057;
        }

        .stars {
            color: #ffc107;
            margin-bottom: 10px;
        }

        .box h3 {
            font-size: 22px;
            color: #343a40;
            margin-bottom: 10px;
        }

        .footer {
            background: linear-gradient(to right, #343a40, #2c3036);
            color: #ffffff;
            text-align: center;
            padding: 40px 0;
            position: relative;
            bottom: 0;
            width: 100%;
            margin-top: 40px;
        }

        .footer p {
            font-size: 18px;
        }
    </style>
</head>
<body>
    <?php @include 'header.php'; ?>

    <section class="heading">
        <h3>About Us</h3>
        <p> <a href="home.php">Home</a> / About Us </p>
    </section>

    <section class="about">
        <div class="flex">
            <div class="content">
                <h3>Why Choose Us?</h3>
                <p>At our store, we are passionate about sneakers, and we believe in offering the best. We provide a wide range of high-quality sneakers, allowing you to express your style and feel comfortable every day.</p>
                <a href="shop.php" class="btn">Shop Now</a>
            </div>
        </div>

        <div class="flex">
            <div class="content">
                <h3>What We Provide?</h3>
                <p>We offer a selection of sneakers from top brands in the industry. Additionally, we have a dedicated team of fashion experts who can help you choose the perfect pair. We are here to help you stand out and feel confident every day.</p>
                <a href="contact.php" class="btn">Contact Us</a>
            </div>
        </div>

        <div class="flex">
            <div class="content">
                <h3>Who We Are?</h3>
                <p>We are a team of sneaker enthusiasts with a common vision: to bring you the coolest and most innovative sneakers. With years of experience in the industry, we're here to help you find the perfect sneakers and provide you with the best service.</p>
                <a href="#reviews" class="btn">Client Reviews</a>
            </div>
        </div>
    </section>

    <section class="reviews" id="reviews">
        <h1 class="title">Client Reviews</h1>
        <div class="box-container">
            <div class="box">
                <img src="images/pic-1.png" alt="Satisfied Customer">
                <p>"I bought the most comfortable and stylish pair of sneakers here. I'm very satisfied with the service and quality!"</p>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <h3>John Doe</h3>
            </div>
            <div class="box">
                <img src="images/pic-2.png" alt="Happy Customer">
                <p>"I found the perfect sneakers for my style at this store. Great selection and friendly staff!"</p>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <h3>Jane Smith</h3>
            </div>
            <div class="box">
                <img src="images/pic-3.png" alt="Satisfied Shopper">
                <p>"I always come back to this store for the latest sneaker releases. The quality is top-notch!"</p>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <h3>Chris Johnson</h3>
            </div>
        </div>
    </section>

    <?php @include 'footer.php'; ?>

    <script src="js/script.js"></script>
</body>
</html>
