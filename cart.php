<?php

@include 'config.php';

session_start();

$user_id = $_SESSION['user_id'];

if (!isset($user_id)) {
    header('location:login.php');
}

if (isset($_POST['update_quantity'])) {
    $cart_id = $_POST['cart_id'];
    $cart_quantity = $_POST['cart_quantity'];
    mysqli_query($conn, "UPDATE `cart` SET quantity = '$cart_quantity' WHERE id = '$cart_id'") or die('query failed');
    $message[] = 'cart quantity updated!';
}

// Delete Product from Cart Logic
if (isset($_GET['delete'])) {
    $delete_id = $_GET['delete'];
    mysqli_query($conn, "DELETE FROM `cart` WHERE id = '$delete_id'") or die('query failed');
    header('location:cart.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shopping Cart</title>

    <!-- Font Awesome CDN link  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">

    <!-- Custom admin CSS file link  -->
    <link rel="stylesheet" href="css/style.css">

    <style>
        body {
            font-family: 'Arial', sans-serif;
            font-size: 16px;
            line-height: 1.6;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
            color: #333;
        }

        header {
            background-color: #333;
            color: #fff;
            padding: 15px;
            text-align: center;
            font-size: 24px;
        }

        section {
            padding: 20px;
            margin: 20px;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        .heading h3 {
            font-size: 30px;
            margin-bottom: 10px;
        }

        .heading p {
            font-size: 16px;
            color: #777;
        }

        .shopping-cart .title {
            font-size: 24px;
            margin-bottom: 20px;
        }

        .box-container {
            display: flex;
            flex-wrap: wrap;
            gap: 20px;
        }

        .box {
            position: relative;
            border: 1px solid #ddd;
            padding: 15px;
            border-radius: 8px;
            width: 100%;
            transition: transform 0.2s ease-in-out;
        }

        .box:hover {
            transform: scale(1.05);
        }

        .box a {
            font-size: 20px;
            color: #333;
            text-decoration: none;
            transition: color 0.3s ease-in-out;
        }

        .box:hover a {
            color: #ff5252;
        }

        .box .view-btn {
            position: absolute;
            top: 10px;
            right: 40px;
        }

        .box .delete-btn {
            position: absolute;
            top: 10px;
            right: 10px;
        }

        .image {
            width: 100%;
            height: 100%;
            max-width: 450px;
            max-height: 450px;
            object-fit: cover;
            margin-bottom: 10px;
            border-radius: 8px;
        }

        .name {
            font-size: 20px;
            margin-bottom: 8px;
        }

        .size {
            font-size: 16px;
            margin-bottom: 8px;
        }

        .price {
            font-size: 20px;
            margin-bottom: 8px;
        }

        form {
            display: flex;
            gap: 10px;
            align-items: center;
            margin-bottom: 10px;
        }

        .qty {
            width: 60px;
            padding: 5px;
            font-size: 16px;
            border: 1px solid #ddd;
            border-radius: 4px;
        }

        .view-btn,
        .delete-btn,
        .option-btn {
            font-size: 16px;
            padding: 8px;
            background-color: #333;
            color: #fff;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            transition: background-color 0.3s ease-in-out;
        }

        .view-btn:hover,
        .delete-btn:hover,
        .option-btn:hover {
            background-color: #ff5252;
        }

        .delete-btn.disabled {
            background-color: #ccc;
            cursor: not-allowed;
        }

        .delete-btn:hover:not(.disabled) {
            background-color: #c70039;
        }

        .cart-total,
        .more-btn {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-top: 20px;
            font-size: 20px;
        }

        .cart-total p {
            margin: 0;
        }

        .option-btn.disabled {
            background-color: #ccc;
            cursor: not-allowed;
        }

        .option-btn:hover:not(.disabled) {
            background-color: #45a049;
        }

        .empty {
            font-size: 20px;
            color: #777;
        }

        footer {
            background-color: #333;
            color: #fff;
            padding: 15px;
            text-align: center;
            position: absolute;
            bottom: 0;
            width: 100%;
        }
    </style>
</head>

<body>

    <?php @include 'header.php'; ?>

    <section class="heading">
        <h3>Shopping Cart</h3>
        <p><a href="home.php">Home</a> / Cart</p>
    </section>

    <section class="shopping-cart">

        <h1 class="title">Products Added</h1>

        <div class="box-container">
            <?php
            $grand_total = 0;
            $select_cart = mysqli_query($conn, "SELECT * FROM `cart` WHERE user_id = '$user_id'") or die('Query failed');
            if (mysqli_num_rows($select_cart) > 0) {
                while ($fetch_cart = mysqli_fetch_assoc($select_cart)) {
            ?>
                    <div class="box">
                        <a href="view_page.php?pid=<?php echo $fetch_cart['pid']; ?>" class="fas fa-eye view-btn"></a>
                        <a href="cart.php?delete=<?php echo $fetch_cart['id']; ?>" class="fas fa-times delete-btn" onclick="return confirm('Delete this from the cart?');"></a>
                        <img src="uploaded_img/<?php echo $fetch_cart['image']; ?>" alt="" class="image">
                        <div class="name"><?php echo $fetch_cart['name']; ?></div>
                        <div class="size"><?php echo $fetch_cart['size']; ?></div>
                        <div class="price">$<?php echo $fetch_cart['price']; ?>/-</div>
                        <form action="" method="post">
                            <input type="hidden" value="<?php echo $fetch_cart['id']; ?>" name="cart_id">
                            <input type="number" min="1" value="<?php echo $fetch_cart['quantity']; ?>" name="cart_quantity" class="qty">
                            <input type="submit" value="Update" class="option-btn" name="update_quantity">
                        </form>
                        <div class="sub-total">Sub-Total: <span>$<?php echo $sub_total = ($fetch_cart['price'] * $fetch_cart['quantity']); ?>/-</span></div>
                    </div>
            <?php
                    $grand_total += $sub_total;
                }
            } else {
                echo '<p class="empty">Your cart is empty</p>';
            }
            ?>
        </div>

        <div class="more-btn">
            <a href="cart.php?delete_all" class="delete-btn <?php echo ($grand_total > 1) ? '' : 'disabled' ?>" onclick="return confirm('Delete all items from the cart?');">Delete All</a>
        </div>

        <div class="cart-total">
            <p>Grand Total: <span>$<?php echo $grand_total; ?>/-</span></p>
            <a href="shop.php" class="option-btn">Continue Shopping</a>
            <a href="checkout.php" class="option-btn <?php echo ($grand_total > 1) ? '' : 'disabled' ?>">Proceed to Checkout</a>
        </div>

    </section>

    <?php @include 'footer.php'; ?>

    <script src="js/script.js"></script>

</body>

</html>
